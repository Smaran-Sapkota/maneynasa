###############################
#
# robots.txt file for this website 
#
# addresses all robots by using wild card *
#
User-agent: *
# list folders robots are not allowed to index

Disallow: /admin/
Disallow: /public/
Disallow: /ASCIcore_1_7_2/
Disallow: /components/

###############################